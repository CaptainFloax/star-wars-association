# Contexte

Vous travaillez pour PipoESN®, une société de services qui conseille et réalise des applications mobiles pour des clients de type entreprise ayant un profil PME.

Justement une association de fans de “Star Wars” vient de vous contacter pour réaliser une application mobile pour délivrer un maximum d’informations au sujet de cette fameuse saga.

# Besoin

L’application a pour objectif d’être visible sur les principaux stores de manière publique et gratuite.

Elle aura 2 objectifs :
Faire connaître la série aux néophytes et leur donner envie de visionner la saga.
Faire parler de l’association afin que de nouveaux fans viennent adhérer à l’association.

L’association a des contraintes fortes sur la réalisation de cette application :
Elle ne dispose pas d’un gros budget.
Elle n’a aucun SI sur lequel s’appuyer pour réaliser l’application et elle ne souhaite pas investir dans une éventuelle infogérance.
Elle souhaite que l’application soit accessible avant la sortie du film n°8 le 13 décembre 2017 pour profiter de la notoriété de cette sortie au cinéma.

NB : Étant donné la date très rapprochée de la sortie du film, le commercial de PipoESN® a réussi à négocier un délai supplémentaire pour que celle-ci soit réalisée avant les vacances scolaires de Noël, c’est à dire pour une livraison le vendredi 22 soir au plus tard.

# Votre mission

Vous êtes la crème de la crème de la société PipoESN® (et accessoirement les seuls disponibles actuellement dans l’entreprise), on compte donc sur vous pour mener à bien cette mission en étant force de proposition pour répondre au besoin du client et lui proposer une solution et un planning de réalisation.

Le directeur technique de PipoESN® a lu ce CDC rédigé sur un post-it et il vous invite à regarder les liens suivants pour mener à bien votre mission :

Ionic pour la partie “front-end” : https://ionicframework.com/
The Star Wars API pour la partie “back-end” : https://swapi.co/

Bien évidemment la réussite du projet passera par le fait que les utilisateurs aient un réel intérêt à utiliser cette application, faites que cela soit simple et ludique pour eux.
