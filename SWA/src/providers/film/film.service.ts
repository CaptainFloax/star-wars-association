import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Film } from '../../models/film';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FilmProvider {

  constructor(private http: HttpClient){}
  
    getFilm(id: Number): Observable<Film>{
      return this.http.get<Film>("https://swapi.co/api/films/" + id);        
    }
    
    showFilmList(page: Number): Observable<Film> {
      return this.http.get<Film>("https://swapi.co/api/films/?page=" + page);
    }

}
