import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { modelJson } from '../../models/modelJson';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  constructor(public http: HttpClient) { }

  getFromUrl(url: string): Observable<any>{
    return this.http.get<any>(url);        
  }
  
  getFromUrlModel(url: string): Observable<modelJson>{
    return this.http.get<modelJson>(url);        
  }

}
