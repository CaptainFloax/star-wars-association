import { Injectable } from '@angular/core';
import { People } from '../../models/people';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PeopleProvider {

  constructor(private http:HttpClient) {}
  
    getPeople(id: Number): Observable<People> {
      return this.http.get<People>("https://swapi.co/api/people/" + id);
    }
  
    showPeopleList(page: Number): Observable<People> {
      return this.http.get<People>("https://swapi.co/api/people/?page="+ page );
    }

}
