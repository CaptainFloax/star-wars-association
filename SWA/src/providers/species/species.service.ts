import { Injectable } from '@angular/core';
import { Species } from  '../../models/species';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SpeciesProvider {

  constructor(private http:HttpClient) {}
  
    getSpecies(id: Number): Observable<Species> {
      return this.http.get<Species>("https://swapi.co/api/species/" + id);
    }
  
    showSpeciesList(page: Number):Observable<Species> {
      return this.http.get<Species>("https://swapi.co/api/species/?page="+ page );
    }

}
