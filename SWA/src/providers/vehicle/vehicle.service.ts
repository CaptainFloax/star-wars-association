import { Injectable } from '@angular/core';
import { Vehicle } from '../../models/vehicle';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class VehicleProvider {

  constructor(private http: HttpClient){}
  
    getVehicle(id: Number): Observable<Vehicle>{
      return this.http.get<Vehicle>("https://swapi.co/api/vehicles/" + id);
    }
    
    showVehicleList(page: Number):Observable<Vehicle> {
      return this.http.get<Vehicle>("https://swapi.co/api/vehicles/?page="+page ) ;
    }

}
