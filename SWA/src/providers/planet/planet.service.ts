import { Injectable } from '@angular/core';
import { Planet } from '../../models/planet'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PlanetProvider {

  constructor(private http:HttpClient) {}
  
    getPlanet(id: Number): Observable<Planet> {
      return this.http.get<Planet>("https://swapi.co/api/planets/" + id)
    }
  
    showPlanetList(page: Number): Observable<Planet> {
      return this.http.get<Planet>("https://swapi.co/api/planets/?page=" + page );
    }

}
