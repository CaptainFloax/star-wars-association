import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SwaaagProvider {

  constructor(private http: HttpClient){}
  
    getSlides(): Observable<any>{
      return this.http.get<any>("assets/data/data.json");
    }

}
