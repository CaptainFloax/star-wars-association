import { Injectable } from '@angular/core';
import { Starship } from '../../models/starship';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class StarshipProvider {

  constructor(private http: HttpClient){}
  
    getStarship(id: Number): Observable<Starship>{
      return this.http.get<Starship>("https://swapi.co/api/starships/" + id);
    }
    
    showStarshipList(page: Number):Observable<Starship> {
      return this.http.get<Starship>("https://swapi.co/api/starships/?page="+page );
    }

}
