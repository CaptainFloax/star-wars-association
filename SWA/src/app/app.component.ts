import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { Helper } from '../models/helper';
import { ApiProvider } from '../providers/api/api.service';
import { FilmProvider } from '../providers/film/film.service';
import { PeopleProvider } from '../providers/people/people.service';
import { PlanetProvider } from '../providers/planet/planet.service';
import { SpeciesProvider } from '../providers/species/species.service';
import { StarshipProvider } from '../providers/starship/starship.service';
import { VehicleProvider } from '../providers/vehicle/vehicle.service';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, 
    public storage: Storage,
    public apiProvider: ApiProvider,
    public filmProvider: FilmProvider,
    public peopleProvider: PeopleProvider,
    public planetProvider: PlanetProvider,
    public speciesProvider: SpeciesProvider,
    public starshipProvider: StarshipProvider,
    public vehicleProvider: VehicleProvider) {
    platform.ready().then(() => {
      this.getHelper();
    }).then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  async getHelper() {
    Helper.apiProvider = this.apiProvider;
    Helper.filmProvider = this.filmProvider;
    Helper.peopleProvider = this.peopleProvider;
    Helper.planetProvider = this.planetProvider;
    Helper.speciesProvider = this.speciesProvider;
    Helper.starshipProvider = this.starshipProvider;
    Helper.vehicleProvider = this.vehicleProvider;
    Helper.storage = this.storage;
    await Helper.LoadAllData();
  }
}
