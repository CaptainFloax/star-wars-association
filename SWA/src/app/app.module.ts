import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';

import { AboutPage } from '../pages/about/about';
import { WikiPage } from '../pages/wiki/wiki';
import { QuizPage } from '../pages/quiz/quiz';
import { TabsPage } from '../pages/tabs/tabs';
import { WikiFilmsPage } from '../pages/wiki/films/films.wiki';
import { WikiPeoplePage } from '../pages/wiki/people/people.wiki';
import { WikiPlanetsPage } from '../pages/wiki/planets/planets.wiki';
import { WikiSpeciesPage } from '../pages/wiki/species/species.wiki';
import { WikiStarshipsPage } from '../pages/wiki/starships/starships.wiki';
import { WikiVehiclesPage } from '../pages/wiki/vehicles/vehicles.wiki';
import { WikiDetailPage } from '../pages/wiki-detail/wiki-detail';

import { HeadBarComponent } from '../components/head-bar/head-bar.component';
import { WikiCardsComponent } from '../components/wiki-cards/wiki-cards.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FilmProvider } from '../providers/film/film.service';
import { PeopleProvider } from '../providers/people/people.service';
import { VehicleProvider } from '../providers/vehicle/vehicle.service';
import { StarshipProvider } from '../providers/starship/starship.service';
import { SpeciesProvider } from '../providers/species/species.service';
import { PlanetProvider } from '../providers/planet/planet.service';
import { ApiProvider } from '../providers/api/api.service';
import { SwaaagProvider } from '../providers/swaaag.service';

import { HttpClientModule } from '@angular/common/http';
 

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    WikiPage,
    QuizPage,
    TabsPage,
    WikiFilmsPage,
    WikiPeoplePage,
    WikiPlanetsPage,
    WikiSpeciesPage,
    WikiStarshipsPage,
    WikiVehiclesPage,
    WikiDetailPage,
    HeadBarComponent,
    WikiCardsComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    WikiPage,
    QuizPage,
    TabsPage,
    WikiFilmsPage,
    WikiPeoplePage,
    WikiPlanetsPage,
    WikiSpeciesPage,
    WikiStarshipsPage,
    WikiVehiclesPage,
    WikiDetailPage,
    HeadBarComponent,
    WikiCardsComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FilmProvider,
    PeopleProvider,
    VehicleProvider,
    StarshipProvider,
    SpeciesProvider,
    StarshipProvider,
    PlanetProvider,
    ApiProvider,
    SwaaagProvider
  ]
})
export class AppModule {}
