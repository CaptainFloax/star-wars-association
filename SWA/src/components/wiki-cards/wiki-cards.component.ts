import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'wiki-cards',
  templateUrl: 'wiki-cards.component.html'
})

export class WikiCardsComponent {

  @Input() slide: any;

  constructor(private sanitizer: DomSanitizer) {

  }

  getBackground(image) {
    return this.sanitizer.bypassSecurityTrustStyle(`linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.4)), url(${image})`);
  }
}
