import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { WikiPage } from '../wiki/wiki';
import { QuizPage } from '../quiz/quiz';

@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {

  tab1Root = QuizPage;
  tab2Root = WikiPage;
  tab3Root = AboutPage;

  constructor() {

  }
}
