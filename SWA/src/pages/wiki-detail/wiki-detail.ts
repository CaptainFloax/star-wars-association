import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'wiki-detail',
  templateUrl: 'wiki-detail.html'
})
export class WikiDetailPage {
	public valueOfDisplay: String = 'none';
  public valueOfDisplayInfos: String = 'block';
  public valueOfDisplayPeople: String = 'none';
  public valueOfDisplaySpecies: String = 'none';

  public selectedColor: String = 'Blue';
  public defaultColor: String = "Black";

  constructor(public navCtrl: NavController) {

  }
  buttonClick(){
  	if( this.valueOfDisplay ==  'none'){
  		this.valueOfDisplay = "block";
  	}else{
  		this.valueOfDisplay = "none";
  	}
  }
  openPart(typeOfItem){
        this.valueOfDisplayInfos = "none";
        this.valueOfDisplayPeople = "none";
        this.valueOfDisplaySpecies = "none";
    switch (typeOfItem) {
      case "Infos":
        this.valueOfDisplayInfos = "block";
      break;
      case "People":
        this.valueOfDisplayPeople = "block";
      break;
      case "Species":
        this.valueOfDisplaySpecies = "block";
      break;
      default:
      break;
    }
  }
}
