import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Quiz } from '../../models/quiz';
import { PeopleProvider } from '../../providers/people/people.service';
import { ApiProvider } from '../../providers/api/api.service'
import { FilmProvider } from '../../providers/film/film.service';
import { Variable } from '../../models/variable';
import { People } from '../../models/people';
import { PlanetProvider } from '../../providers/planet/planet.service';
import { StarshipProvider } from '../../providers/starship/starship.service';
import { VehicleProvider } from '../../providers/vehicle/vehicle.service';
import { SpeciesProvider } from '../../providers/species/species.service';
import { Film } from '../../models/film';
import { get } from '@ionic-native/core';
import { Helper } from '../../models/helper';

/**
 * Generated class for the QuizPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-quiz',
  templateUrl: 'quiz.html',
})
export class QuizPage {
  
  private questionNumber: number;
  private goodAnswerUser: number;
  public visibilityStart: boolean = true;
  public VisibilityQuestion: boolean = false;
  public VisibilityFinal: boolean = false;

  public quiz: Quiz;

  
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private peopleProvider: PeopleProvider, private apiProvider: ApiProvider, private filmProvider: FilmProvider, private planetProvider: PlanetProvider,
    private starshipProvider: StarshipProvider, private vehicleProvider : VehicleProvider, private speciesProvider : SpeciesProvider) {
  }

  ngOnInit() {
    this.questionNumber = 1;
    this.goodAnswerUser = 0;
  
      this.quiz = new Quiz();
  }

  onButtonStartClicked(event) {
    this.visibilityStart = !this.visibilityStart;
    this.VisibilityQuestion = !this.VisibilityQuestion;
    this.quiz.DisplayQuestion1();
    this.questionNumber++;
  }  

  onButtonRestartClicked(event) {
    this.visibilityStart = !this.visibilityStart;
    this.VisibilityFinal = !this.VisibilityFinal;

    this.questionNumber = 1;
    this.goodAnswerUser = 0;
  }

  onSelectItem(answser: string) {
    switch(answser)
    {
      case '1':
      if(this.quiz.answer1 == this.quiz.goodAnswer)
      {
        this.goodAnswerUser++;
        alert("TRUE");
      }
      else
      {
        alert("FALSE");
      }

      break;
      case '2':
      if(this.quiz.answer2 == this.quiz.goodAnswer)
      {
        this.goodAnswerUser++;
        alert("TRUE");
      }
      else
      {
        alert("FALSE");
      }

      break;
      case '3':
      if(this.quiz.answer3 == this.quiz.goodAnswer)
      {
        this.goodAnswerUser++;
        alert("TRUE");
      }
      else
      {
        alert("FALSE");
      }

      break;
      case '4':
      if(this.quiz.answer4 == this.quiz.goodAnswer)
      {
        this.goodAnswerUser++;
        alert("TRUE");
      }
      else
      {
        alert("FALSE");
      }

      break;
      default: break;
    }

    switch(this.questionNumber)
    {
      case 2: 
        this.quiz.DisplayQuestion2();
        break;
      case 3:
        this.quiz.DisplayQuestion3();
        break;
      case 4:
        this.quiz.DisplayQuestion4();
        break;
      case 5:
        this.quiz.DisplayQuestion5();
        break;
      case 6:
        this.quiz.DisplayQuestion6();
        break;
      case 7:
        this.quiz.DisplayQuestion7();
        break;
      case 8:
        this.quiz.DisplayQuestion8();
        break;
      case 9:
        this.quiz.DisplayQuestion9();
        break;
      case 10:
        this.quiz.DisplayQuestion10();
        break;
      case 11:
        this.VisibilityQuestion = !this.VisibilityQuestion;
        this.VisibilityFinal = !this.VisibilityFinal;
        break;
    }

    this.questionNumber++
  }
}
