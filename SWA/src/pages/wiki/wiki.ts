import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { VehicleProvider } from '../../providers/vehicle/vehicle.service';
import { PeopleProvider } from '../../providers/people/people.service';
import { FilmProvider } from '../../providers/film/film.service';
import { PlanetProvider } from '../../providers/planet/planet.service';
import { SpeciesProvider } from '../../providers/species/species.service';
import { StarshipProvider } from '../../providers/starship/starship.service';
import { SwaaagProvider } from '../../providers/swaaag.service';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Variable } from '../../models/variable';
import 'rxjs/add/operator/concat';
import { WikiFilmsPage } from './films/films.wiki';
import { WikiPeoplePage } from './people/people.wiki';
import { WikiPlanetsPage } from './planets/planets.wiki';
import { WikiSpeciesPage } from './species/species.wiki';
import { WikiVehiclesPage } from './vehicles/vehicles.wiki';
import { WikiStarshipsPage } from './starships/starships.wiki';

@Component({
  selector: 'page-wiki',
  templateUrl: 'wiki.html',
})
export class WikiPage {
  public countFilms: Number = 0;
  public countPeople: Number = 0;
  public countPlanets: Number = 0;
  public countSpecies: Number = 0;
  public countVehicles: Number = 0;
  public countStarships: Number = 0;
  public slideIndex: Number = 0;
  public bgImg: string = "assets/imgs/cards/cards_film.jpg";
  public slides: Observable<any>[] = [];

  @ViewChild('catSlider') slider:  Slides;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public swaaagProvider: SwaaagProvider) {}

  ngOnInit() {
    this.slides.push(this.swaaagProvider.getSlides());
    this.countFilms = Variable.filmJson.count;
    this.countPeople = Variable.peopleJson.count;
    this.countPlanets = Variable.planetJson.count;
    this.countSpecies = Variable.speciesJson.count;
    this.countVehicles = Variable.vehicleJson.count;
    this.countStarships = Variable.starshipJson.count;
  }

  getBackground(index){
    this.slides[0].subscribe(x => {
      console.log(x.slides[index].img);
      this.bgImg = x.slides[index].img;
    });
  }

  onSlideChanged(){
    this.slideIndex = this.slider.getActiveIndex();
    this.getBackground(this.slideIndex);
  }

  goToList() {
    this.slideIndex = this.slider.getActiveIndex();
    switch(this.slideIndex) { 
      case 0: { 
        this.navCtrl.push(WikiFilmsPage);
        break; 
      } 
      case 1: { 
        this.navCtrl.push(WikiPeoplePage);
        break; 
      } 
      case 2: { 
        this.navCtrl.push(WikiPlanetsPage);
        break; 
      } 
      case 3: { 
        this.navCtrl.push(WikiSpeciesPage);
        break; 
      } 
      case 4: { 
        this.navCtrl.push(WikiVehiclesPage);
        break; 
      } 
      case 5: { 
        this.navCtrl.push(WikiStarshipsPage);
        break; 
      } 
      default: { 
        break; 
      } 
    }
  }

}
