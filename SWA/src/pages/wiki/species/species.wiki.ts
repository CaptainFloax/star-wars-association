import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Variable } from '../../../models/variable';
import { Observable } from 'rxjs/Observable';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import 'rxjs/add/operator/concat';
import { Species } from '../../../models/species';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Helper } from '../../../models/helper';

@Component({
  selector: 'page-wiki-species',
  templateUrl: 'species.wiki.html',
})
export class WikiSpeciesPage {
  public results: Species[] = [];

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {}

  ngOnInit() {
    this.results = Variable.species;
  }
  
  async refresh() {
    let loading = this.loadingCtrl.create({
        content: 'Please wait, We are trying to refresh data...'
    });
    let toast = this.toastCtrl.create({
        message: 'Data were succesfully refreshed',
        duration: 3000
    });
    
    loading.present();

    await Helper.LoadAllSpecies(true);
    this.results = Variable.species;
    
    await loading.dismiss();

    toast.present();
  }

}
