import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Variable } from '../../../models/variable';
import { Observable } from 'rxjs/Observable';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import 'rxjs/add/operator/concat';
import { Planet } from '../../../models/planet';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Helper } from '../../../models/helper';

@Component({
  selector: 'page-wiki-planets',
  templateUrl: 'planets.wiki.html',
})
export class WikiPlanetsPage {
  public results: Planet[] = [];

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {}

  ngOnInit() {
    this.results = Variable.planets;
  }
  
  async refresh() {
    let loading = this.loadingCtrl.create({
        content: 'Please wait, We are trying to refresh data...'
    });
    let toast = this.toastCtrl.create({
        message: 'Data were succesfully refreshed',
        duration: 3000
    });
    
    loading.present();

    await Helper.LoadAllPlanets(true);
    this.results = Variable.planets;
    
    await loading.dismiss();

    toast.present();
  }

}
