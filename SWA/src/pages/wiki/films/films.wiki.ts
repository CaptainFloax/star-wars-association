import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Variable } from '../../../models/variable';
import { Film } from '../../../models/film';
import { Observable } from 'rxjs/Observable';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import 'rxjs/add/operator/concat';
import { Helper } from '../../../models/helper';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

@Component({
  selector: 'page-wiki-films',
  templateUrl: 'films.wiki.html',
})
export class WikiFilmsPage {
  public results: Film[] = [];

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {}

  ngOnInit() {
    this.results = Variable.films;
  }

  async refresh() {
    let loading = this.loadingCtrl.create({
        content: 'Please wait, We are trying to refresh data...'
    });
    let toast = this.toastCtrl.create({
        message: 'Data were succesfully refreshed',
        duration: 3000
    });
    
    loading.present();

    await Helper.LoadAllFilms(true);
    this.results = Variable.films;
    
    await loading.dismiss();

    toast.present();
  }

}
