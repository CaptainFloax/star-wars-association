import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Variable } from '../../../models/variable';
import { Observable } from 'rxjs/Observable';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import 'rxjs/add/operator/concat';
import { People } from '../../../models/people';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Helper } from '../../../models/helper';
import { Film } from '../../../models/film';

@Component({
  selector: 'page-wiki-people',
  templateUrl: 'people.wiki.html',
})
export class WikiPeoplePage {
  public results: People[] = [];
  public showDetails: String = "";

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {}

  ngOnInit() {
    this.results = Variable.people;
  }
  
  async refresh() {
    let loading = this.loadingCtrl.create({
        content: 'Please wait, We are trying to refresh data...'
    });
    let toast = this.toastCtrl.create({
        message: 'Data were succesfully refreshed',
        duration: 3000
    });
    
    loading.present();

    await Helper.LoadAllPeople(true);
    this.results = Variable.people;
    
    await loading.dismiss();

    toast.present();
  }

  toggleCard(peopleUrl: String) {
    this.showDetails == peopleUrl ? this.showDetails = "" : this.showDetails = peopleUrl;
  }

  getFilm(filmUrl: string): Film{
    return Variable.GetFilmByUrl(filmUrl);
  }

}
