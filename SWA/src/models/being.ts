export class Being {
    name: string;
    skin_colors: string;
    hair_colors: string;
    eye_colors: string; 
    homeworld: string;
    films: string[];
    url: string;
    
    constructor() {
        this.name = null;
        this.skin_colors = null;
        this.hair_colors = null;
        this.eye_colors = null;
        this.homeworld = null;
        this.films = null;
        this.url = null;
    }
}