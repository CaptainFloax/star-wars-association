export class Planet {

    name: string;
	rotation_period: number;
	orbital_period: number;
	diameter: number;
	climate: string;
	gravity: string;
	terrain: string;
	surface_water:number;
	population: number;
	residents: string[];
	films: string[];
    url: string;

	constructor(){
		this.name = null;
		this.rotation_period = null;
		this.orbital_period = null;
		this.diameter = null; 
		this.climate = null; 
		this.gravity = null;
		this.terrain = null;
		this.surface_water = null;
		this.population = null;
		this.residents = null;
		this.films = null;
        this.url = null;
	}
}