export class Vehicle {
    name: string;
	model: string;
	manufacturer: string;
	cost_in_credits: number;
	length: number;
	max_atmosphering_speed: number;
	crew: number;
	passengers: number;
	cargo_capacity: number;
	consumables: string;	
	pilots: string[];
	films: string[];
    url: string;
	vehicle_class: string;

	constructor(){
		this.name = null;
		this.model = null;
		this.manufacturer = null;
		this.cost_in_credits = null;
		this.length = null;
		this.max_atmosphering_speed = null;
		this.crew = null;
		this.passengers = null;
		this.cargo_capacity = null;
		this.consumables = null;
		this.vehicle_class = null;
		this.pilots = null;
		this.films = null;
        this.url = null;
	}
}