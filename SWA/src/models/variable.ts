import { modelJson } from "./modelJson";
import { Film } from "./film";
import { People } from "./people";
import { Planet } from "./planet";
import { Species } from "./species";
import { Vehicle } from "./vehicle";
import { Starship } from "./starship";

export class Variable{
    public static filmJson: modelJson;
    public static peopleJson: modelJson;
    public static planetJson: modelJson;
    public static speciesJson: modelJson;
    public static vehicleJson: modelJson;
    public static starshipJson: modelJson;

    public static films: Film[];
    public static people: People[];
    public static planets: Planet[];
    public static species: Species[];
    public static vehicles: Vehicle[];
    public static starships: Starship[];

    public static GetFilmByUrl(url: string): Film
    {
        return this.films.filter(item => item.url == url)[0];
    }

    public static GetPeopleByUrl(url: string): People
    {
        return this.people.filter(item => item.url == url)[0];
    }

    public static GetPlanetByUrl(url: string): Planet
    {
        return this.planets.filter(item => item.url == url)[0];
    }

    public static GetSpeciesByUrl(url: string): Species
    {
        return this.species.filter(item => item.url == url)[0];
    }

    public static GetVehicleByUrl(url: string): Vehicle
    {
        return this.vehicles.filter(item => item.url == url)[0];
    }
    
    public static GetStarshipByUrl(url: string): Starship
    {
        return this.starships.filter(item => item.url == url)[0];
    }

}