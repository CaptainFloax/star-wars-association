import {Vehicle} from './vehicle';

export class Starship extends Vehicle {
  	hyperdrive_rating: number;
	MGLT: number;
	starship_class: string;

	constructor() {
		super();
		this.hyperdrive_rating = null;
		this.MGLT = null;
		this.starship_class = null;
	}
}