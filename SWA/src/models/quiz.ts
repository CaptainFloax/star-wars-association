import { People } from "./people";
import { Variable } from "./variable";
import { Helper } from "./helper";
import { Film } from "./film";
import { Starship } from "./starship";
import { Planet } from "./planet";
import { Vehicle } from "./vehicle";
import { Species } from "./species";

export class Quiz {
    question: string;
    answer1: string;
    answer2: string;
    answer3: string;
    answer4: string;
    goodAnswer: string;
    urlTemplate: string = "https://swapi.co/api/";

    private templateQuestions: string[] = [
        "Movie {personnage} ?",
        "{starship} cost_in_credits ?",
        "{planet} climate",
        "manufacturer{vehicle}",
        "language {species}",
        "Dans l'épisode {film}, quelle espèce est présente?",
        "{personnage} de quel espèce",
        "Number of passenger {starship}",
        "{vehicle} vehicle_class",
        "classification {species}"    
    ];
  
      
    constructor() {
        this.question = null;
        this.answer1 = null;
        this.answer2 = null;
        this.answer3 = null;
        this.answer4 = null;
        this.goodAnswer = null;
    }

    public DisplayQuestion1(){  

        let people: People;

        do {
            people = Variable.people[Helper.GetRandomNumber(0, Variable.peopleJson.count - 1)]
        }while(Variable.filmJson.count - people.films.length < 3)

        this.question = this.templateQuestions[0].replace("{personnage}", people.name);

        let urlFilmGoodAnswerArray: string[] = people.films;
        let idFilmGoodAnswerArray: number[] = [];
        
        urlFilmGoodAnswerArray.forEach(item => {
            idFilmGoodAnswerArray.push(parseInt(item.split("/")[5]));
        });

        let wrongAnswerArray = this.GetRandomNumberAnswer(idFilmGoodAnswerArray, Variable.filmJson.count);
        let idFilmAnswer1 = wrongAnswerArray[0];
        let idFilmAnswer2 = wrongAnswerArray[1];
        let idFilmAnswer3 = wrongAnswerArray[2];
        
        let index = Helper.GetRandomNumber(0, urlFilmGoodAnswerArray.length);
        let urlGoodAnswer = this.urlTemplate + "films/" + idFilmGoodAnswerArray[index] + "/";
        this.goodAnswer = Variable.GetFilmByUrl(urlGoodAnswer).title;
        
        let urlAnswer1 = this.urlTemplate + "films/" + idFilmAnswer1 + "/";
        let urlAnswer2 = this.urlTemplate + "films/" + idFilmAnswer2 + "/";
        let urlAnswer3 = this.urlTemplate + "films/" + idFilmAnswer3 + "/";
        let answer1 = Variable.GetFilmByUrl(urlAnswer1).title;
        let answer2 = Variable.GetFilmByUrl(urlAnswer2).title;
        let answer3 = Variable.GetFilmByUrl(urlAnswer3).title;

        let answerArray = Helper.RandomizedAnswer(this.goodAnswer, answer1, answer2, answer3);
        this.answer1 = answerArray[0];
        this.answer2 = answerArray[1];
        this.answer3 = answerArray[2];
        this.answer4 = answerArray[3];
    }

    public DisplayQuestion2(){  
        let starship: Starship = Variable.starships[Helper.GetRandomNumber(0, Variable.starshipJson.count - 1)];
        this.question = this.templateQuestions[1].replace("{starship}", starship.name);
        this.goodAnswer = starship.cost_in_credits.toString();
        
        let startValueArray: number[] = [];
        
        if(this.goodAnswer == 'unknown') {
            startValueArray.push(0);
        }
        else {
            startValueArray.push(starship.cost_in_credits);
        }

        console.log(startValueArray);

        let wrongAnswerArray = this.GetRandomNumberAnswer(startValueArray, startValueArray[0] + 100000);
        let answer1 = wrongAnswerArray[0].toString();1
        let answer2 = wrongAnswerArray[1].toString();
        let answer3 = wrongAnswerArray[2].toString();
        
        let answerArray = Helper.RandomizedAnswer(this.goodAnswer, answer1, answer2, answer3);
        this.answer1 = answerArray[0];
        this.answer2 = answerArray[1];
        this.answer3 = answerArray[2];
        this.answer4 = answerArray[3];
    }

    public DisplayQuestion3(){          

        let planet: Planet = Variable.planets[Helper.GetRandomNumber(0, Variable.planetJson.count - 1)]
        console.log(planet);
        this.goodAnswer = planet.climate;   
        this.question = this.templateQuestions[2].replace("{planet}", planet.name);             
        
        let wrongAnswerArray = this.GetClimatesgAnswer(this.goodAnswer, Variable.planetJson.count);
        let answer1 = wrongAnswerArray[0];
        let answer2 = wrongAnswerArray[1];
        let answer3 = wrongAnswerArray[2];
        
        let answerArray = Helper.RandomizedAnswer(this.goodAnswer, answer1, answer2, answer3);
        this.answer1 = answerArray[0];
        this.answer2 = answerArray[1];
        this.answer3 = answerArray[2];
        this.answer4 = answerArray[3];
    }

    public DisplayQuestion4(){  
        let vehicle: Vehicle = Variable.vehicles[Helper.GetRandomNumber(0, Variable.vehicleJson.count - 1)];
        this.question = this.templateQuestions[3].replace("{vehicle}", vehicle.name);

        this.goodAnswer = vehicle.manufacturer;
        
        let wrongAnswerArray = this.GetManufacturerAnswer(this.goodAnswer, Variable.vehicleJson.count);
        let answer1 = wrongAnswerArray[0];
        let answer2 = wrongAnswerArray[1];
        let answer3 = wrongAnswerArray[2];
        
        let answerArray = Helper.RandomizedAnswer(this.goodAnswer, answer1, answer2, answer3);
        this.answer1 = answerArray[0];
        this.answer2 = answerArray[1];
        this.answer3 = answerArray[2];
        this.answer4 = answerArray[3];
    }

    public DisplayQuestion5(){  
        let species: Species = Variable.species[Helper.GetRandomNumber(0, Variable.speciesJson.count - 1)];
        this.question = this.templateQuestions[4].replace("{species}", species.name);
        this.goodAnswer = species.language;                
        
        let wrongAnswerArray = this.GetLanguagesgAnswer(this.goodAnswer, Variable.speciesJson.count);
        let answer1 = wrongAnswerArray[0];
        let answer2 = wrongAnswerArray[1];
        let answer3 = wrongAnswerArray[2];
        
        let answerArray = Helper.RandomizedAnswer(this.goodAnswer, answer1, answer2, answer3);
        this.answer1 = answerArray[0];
        this.answer2 = answerArray[1];
        this.answer3 = answerArray[2];
        this.answer4 = answerArray[3];
    }

    public DisplayQuestion6(){  
        
        

        let film: Film = Variable.films[Helper.GetRandomNumber(1, Variable.filmJson.count)];
        this.question = this.templateQuestions[5].replace("{film}", film.title);;

        let urlSpeciesGoodAnswerArray: string[] = film.species;
        let idSpeciesGoodAnswerArray: number[] = [];
        
        urlSpeciesGoodAnswerArray.forEach(item => {
            idSpeciesGoodAnswerArray.push(parseInt(item.split("/")[5]));
        });

        let wrongAnswerArray = this.GetRandomNumberAnswer(idSpeciesGoodAnswerArray, Variable.speciesJson.count);
        let idFilmAnswer1 = wrongAnswerArray[0];
        let idFilmAnswer2 = wrongAnswerArray[1];
        let idFilmAnswer3 = wrongAnswerArray[2];
        
        let urlGoodAnswer = this.urlTemplate + "species/" + idSpeciesGoodAnswerArray[Helper.GetRandomNumber(1, urlSpeciesGoodAnswerArray.length)] + "/";
        this.goodAnswer = Variable.GetSpeciesByUrl(urlGoodAnswer).name;
        
        let urlAnswer1 = this.urlTemplate + "species/" + idFilmAnswer1 + "/";
        let urlAnswer2 = this.urlTemplate + "species/" + idFilmAnswer2 + "/";
        let urlAnswer3 = this.urlTemplate + "species/" + idFilmAnswer3 + "/";
        let answer1 = Variable.GetSpeciesByUrl(urlAnswer1).name;
        let answer2 = Variable.GetSpeciesByUrl(urlAnswer2).name;
        let answer3 = Variable.GetSpeciesByUrl(urlAnswer3).name;
        
        let answerArray = Helper.RandomizedAnswer(this.goodAnswer, answer1, answer2, answer3);
        this.answer1 = answerArray[0];
        this.answer2 = answerArray[1];
        this.answer3 = answerArray[2];
        this.answer4 = answerArray[3];
    }

    public DisplayQuestion7(){  
        
        let people: People = Variable.people[Helper.GetRandomNumber(0, Variable.peopleJson.count - 1)];
        this.question = this.templateQuestions[6].replace('{personnage}', people.name);

        let urlSpeciesGoodAnswerArray: string[] = people.species;
        let idSpeciesGoodAnswerArray: number[] = [];
        
        urlSpeciesGoodAnswerArray.forEach(item => {
            idSpeciesGoodAnswerArray.push(parseInt(item.split("/")[5]));
        });

        let wrongAnswerArray = this.GetRandomNumberAnswer(idSpeciesGoodAnswerArray, Variable.speciesJson.count);
        let idFilmAnswer1 = wrongAnswerArray[0];
        let idFilmAnswer2 = wrongAnswerArray[1];
        let idFilmAnswer3 = wrongAnswerArray[2];
        
        let urlGoodAnswer = this.urlTemplate + "species/" + idSpeciesGoodAnswerArray[Helper.GetRandomNumber(0, urlSpeciesGoodAnswerArray.length - 1)] + "/";
        this.goodAnswer = Variable.GetSpeciesByUrl(urlGoodAnswer).name;
        
        let urlAnswer1 = this.urlTemplate + "species/" + idFilmAnswer1 + "/";
        let urlAnswer2 = this.urlTemplate + "species/" + idFilmAnswer2 + "/";
        let urlAnswer3 = this.urlTemplate + "species/" + idFilmAnswer3 + "/";
        let answer1 = Variable.GetSpeciesByUrl(urlAnswer1).name;
        let answer2 = Variable.GetSpeciesByUrl(urlAnswer2).name;
        let answer3 = Variable.GetSpeciesByUrl(urlAnswer3).name;
        
        let answerArray = Helper.RandomizedAnswer(this.goodAnswer, answer1, answer2, answer3);
        this.answer1 = answerArray[0];
        this.answer2 = answerArray[1];
        this.answer3 = answerArray[2];
        this.answer4 = answerArray[3];
    }

    public DisplayQuestion8(){  
        let starship: Starship = Variable.starships[Helper.GetRandomNumber(1, Variable.starshipJson.count)];
        this.question = this.templateQuestions[7].replace('{starship}', starship.name);
        this.goodAnswer = starship.passengers.toString();
        
        let wrongAnswerArray = this.GetRandomNumberAnswer([starship.passengers], starship.passengers + 10);
        let answer1 = wrongAnswerArray[0].toString();
        let answer2 = wrongAnswerArray[1].toString();
        let answer3 = wrongAnswerArray[2].toString();
        
        let answerArray = Helper.RandomizedAnswer(this.goodAnswer, answer1, answer2, answer3);
        this.answer1 = answerArray[0];
        this.answer2 = answerArray[1];
        this.answer3 = answerArray[2];
        this.answer4 = answerArray[3];
    }

    public DisplayQuestion9(){  

        let vehicle: Vehicle = Variable.vehicles[Helper.GetRandomNumber(0, Variable.speciesJson.count - 1)];
        this.question = this.templateQuestions[8].replace("{vehicle}", vehicle.name);

        this.goodAnswer = vehicle.vehicle_class;                
        
        let wrongAnswerArray = this.GetVehiculeClassgAnswer(this.goodAnswer, Variable.vehicleJson.count);
        let answer1 = wrongAnswerArray[0];
        let answer2 = wrongAnswerArray[1];
        let answer3 = wrongAnswerArray[2];
        
        let answerArray = Helper.RandomizedAnswer(this.goodAnswer, answer1, answer2, answer3);
        this.answer1 = answerArray[0];
        this.answer2 = answerArray[1];
        this.answer3 = answerArray[2];
        this.answer4 = answerArray[3];
    }

    public DisplayQuestion10(){  
    
        let species: Species = Variable.species[Helper.GetRandomNumber(1, Variable.speciesJson.count)];
        this.question = this.templateQuestions[9].replace('{species}', species.name);
        
        this.goodAnswer = species.classification;                
        
        let wrongAnswerArray = this.GetClassificationgAnswer(this.goodAnswer, Variable.speciesJson.count);
        let answer1 = wrongAnswerArray[0];
        let answer2 = wrongAnswerArray[1];
        let answer3 = wrongAnswerArray[2];
        
        let answerArray = Helper.RandomizedAnswer(this.goodAnswer, answer1, answer2, answer3);
        this.answer1 = answerArray[0];
        this.answer2 = answerArray[1];
        this.answer3 = answerArray[2];
        this.answer4 = answerArray[3];
    }

    private GetRandomNumberAnswer(idGoodAnswerArray: number[], solutionsNumber: number): number[] {
        let idAnswer1: number;
        let idAnswer2: number;
        let idAnswer3: number;
        let answerFind: boolean = true;
        

        if(idGoodAnswerArray == undefined)
        {
            idAnswer1 = Helper.GetRandomNumber(1, solutionsNumber);

            do {
                idAnswer2 = Helper.GetRandomNumber(1, solutionsNumber);

                if(idAnswer1 != idAnswer2)
                {
                    answerFind = false;
                }
                
            }while(answerFind)

            answerFind = true;
            do {
                idAnswer3 = Helper.GetRandomNumber(1, solutionsNumber);

                if(idAnswer1 != idAnswer3 && idAnswer2 != idAnswer3)
                {
                    answerFind = false;
                }
                
            }while(answerFind)
        }
        else
        {
            do {
                idAnswer1 = Helper.GetRandomNumber(1, solutionsNumber);
                
                if(idGoodAnswerArray.indexOf(idAnswer1) == -1)
                {
                    answerFind = false;
                }
                
            }while(answerFind)


            answerFind = true;
            do {
                idAnswer2 = Helper.GetRandomNumber(1, solutionsNumber);

                if(idGoodAnswerArray.indexOf(idAnswer2) == -1 && idAnswer1 != idAnswer2)
                {
                    answerFind = false;
                }
                
            }while(answerFind)

            answerFind = true;
            do {
                idAnswer3 = Helper.GetRandomNumber(1, solutionsNumber);

                if(idGoodAnswerArray.indexOf(idAnswer3) == -1 && idAnswer1 != idAnswer3 && idAnswer2 != idAnswer3)
                {
                    answerFind = false;
                }
                
            }while(answerFind)
        }

        return [idAnswer1, idAnswer2, idAnswer3];
    } 

    private GetClimatesgAnswer(goodAnswer: string, solutionsNumber: number): string[] {
        let answer1: string;
        let answer2: string;
        let answer3: string;
        let answerFind: boolean = true;
        

        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer1 = Variable.planets[number].climate;

            if(goodAnswer != answer1)
            {
                answerFind = false;
            }
            
        }while(answerFind)


        answerFind = true;
        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer2 = Variable.planets[number].climate;


            if(goodAnswer != answer2 && answer1 != answer2)
            {
                answerFind = false;
            }
            
        }while(answerFind)

        answerFind = true;
        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer3 = Variable.planets[number].climate;

            if(goodAnswer != answer3 && answer3 != answer2 && answer3 != answer1)
            {
                answerFind = false;
            }
            
        }while(answerFind)

        return [answer1, answer2, answer3];
    } 

    private GetLanguagesgAnswer(goodAnswer: string, solutionsNumber: number): string[] {
        let answer1: string;
        let answer2: string;
        let answer3: string;
        let answerFind: boolean = true;
        

        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer1 = Variable.species[number].language;

            if(goodAnswer != answer1)
            {
                answerFind = false;
            }
            
        }while(answerFind)


        answerFind = true;
        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer2 = Variable.species[number].language;


            if(goodAnswer != answer2 && answer1 != answer2)
            {
                answerFind = false;
            }
            
        }while(answerFind)

        answerFind = true;
        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer3 = Variable.species[number].language;

            if(goodAnswer != answer3 && answer3 != answer2 && answer3 != answer1)
            {
                answerFind = false;
            }
            
        }while(answerFind)

        return [answer1, answer2, answer3];
    } 

    private GetVehiculeClassgAnswer(goodAnswer: string, solutionsNumber: number): string[] {
        let answer1: string;
        let answer2: string;
        let answer3: string;
        let answerFind: boolean = true;
        

        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer1 = Variable.vehicles[number].vehicle_class;

            if(goodAnswer != answer1)
            {
                answerFind = false;
            }
            
        }while(answerFind)


        answerFind = true;
        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer2 = Variable.vehicles[number].vehicle_class;


            if(goodAnswer != answer2 && answer1 != answer2)
            {
                answerFind = false;
            }
            
        }while(answerFind)

        answerFind = true;
        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer3 = Variable.vehicles[number].vehicle_class;

            if(goodAnswer != answer3 && answer3 != answer2 && answer3 != answer1)
            {
                answerFind = false;
            }
            
        }while(answerFind)

        return [answer1, answer2, answer3];
    } 

    private GetManufacturerAnswer(goodAnswer: string, solutionsNumber: number): string[] {
        let answer1: string;
        let answer2: string;
        let answer3: string;
        let answerFind: boolean = true;

        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer1 = Variable.vehicles[number].manufacturer;

            if(goodAnswer != answer1)
            {
                answerFind = false;
            }
            
        }while(answerFind)


        answerFind = true;
        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer2 = Variable.vehicles[number].manufacturer;


            if(goodAnswer != answer2 && answer1 != answer2)
            {
                answerFind = false;
            }
            
        }while(answerFind)

        answerFind = true;
        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer3 = Variable.vehicles[number].manufacturer;

            if(goodAnswer != answer3 && answer3 != answer2 && answer3 != answer1)
            {
                answerFind = false;
            }
            
        }while(answerFind)

        return [answer1, answer2, answer3];
    } 

    private GetClassificationgAnswer(goodAnswer: string, solutionsNumber: number): string[] {
        let answer1: string;
        let answer2: string;
        let answer3: string;
        let answerFind: boolean = true;
        

        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer1 = Variable.species[number].classification;

            if(goodAnswer != answer1)
            {
                answerFind = false;
            }
            
        }while(answerFind)


        answerFind = true;
        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer2 = Variable.species[number].classification;


            if(goodAnswer != answer2 && answer1 != answer2)
            {
                answerFind = false;
            }
            
        }while(answerFind)

        answerFind = true;
        do {
            let number = Helper.GetRandomNumber(0, solutionsNumber - 1);
            answer3 = Variable.species[number].classification;

            if(goodAnswer != answer3 && answer3 != answer2 && answer3 != answer1)
            {
                answerFind = false;
            }
            
        }while(answerFind)

        return [answer1, answer2, answer3];
    } 
}