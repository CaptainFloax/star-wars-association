import {Being} from './being';

export class Species extends Being {
	classification: string;
	designation: string;
	average_lifespan: number;
	average_height: number;
	language: string;
	people: string[];	
	
	constructor(){
		super();
		this.classification = null;
		this.designation = null;
		this.average_lifespan = null;
		this.language = null;
		this.people = null;
		this.average_height = null;
	}
}