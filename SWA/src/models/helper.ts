import { ApiProvider } from "../providers/api/api.service";
import { FilmProvider } from "../providers/film/film.service";
import { PeopleProvider } from "../providers/people/people.service";
import { PlanetProvider } from "../providers/planet/planet.service";
import { SpeciesProvider } from "../providers/species/species.service";
import { StarshipProvider } from "../providers/starship/starship.service";
import { VehicleProvider } from "../providers/vehicle/vehicle.service";
import { Variable } from "./variable";
import { Film } from "./film";
import { modelJson } from "./modelJson";
import { VALID } from "@angular/forms/src/model";
import { Storage } from "@ionic/storage";
import { Validators } from "@angular/forms/src/validators";
import { variable } from "@angular/compiler/src/output/output_ast";
import { Vehicle } from "./vehicle";

export class Helper{
    
    public static apiProvider: ApiProvider;
    public static filmProvider: FilmProvider;
    public static peopleProvider: PeopleProvider;
    public static planetProvider: PlanetProvider;
    public static speciesProvider: SpeciesProvider;
    public static starshipProvider: StarshipProvider;
    public static vehicleProvider: VehicleProvider;
    public static storage: Storage;

    
    public static LoadAllFilms(isReload: boolean) {
        if(isReload)
        {
            this.storage.remove('filmJson');
        }

        this.storage.get('filmJson').then(filmJson => {
            if(filmJson == null)
            {
                this.apiProvider.getFromUrlModel("https://swapi.co/api/films/").subscribe(filmJson => {  
                    this.storage.set('filmJson', filmJson).then(() => {
                        Helper.LoadFilmJSon(isReload);
                    });
                });
            }
            else
            {
                Helper.LoadFilmJSon(isReload);
            }   
        });    
    }

    private static LoadFilmJSon(isReload: boolean) {

        this.storage.get('filmJson').then(filmJson => {
            Variable.filmJson = filmJson;
            
            Variable.films = [];
            Variable.filmJson.results.forEach(film => {
                Variable.films.push(film);
            });

            let idPage = null;
            if(Variable.filmJson.next != null) {
                idPage = parseInt(Variable.filmJson.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadFilmsPage(idPage, isReload)
        });
    }

    private static LoadFilmsPage(index: number, isReload: boolean) {
        if(index != null) {

            let nameStorage = 'filmList' + index.toString();
            console.log(nameStorage);

            if(isReload)
            {
                this.storage.remove(nameStorage);
            }
            
            this.storage.get(nameStorage).then(filmList => {
                if(filmList == null)
                {
                    this.filmProvider.showFilmList(index).subscribe(filmJson => {
                        
                        this.storage.set(nameStorage, filmJson).then(() => {
                            Helper.LoadPlanets(nameStorage, isReload);
                        });
                    });
                }
                else
                {
                    Helper.LoadFilms(nameStorage, isReload);
                }
                
            });
        }

       
    }

    private static LoadFilms(nameStorage: string,isReload : boolean) {
        this.storage.get(nameStorage).then(film => {
            film.results.forEach( item => { 
                Variable.films.push(item);
            });         

            let idPage = null;
            if(film.next != null) {
                idPage = parseInt(film.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadFilmsPage(idPage, isReload)

        });
    }

    public static LoadAllPlanets(isReload: boolean) {
        if(isReload)
        {
            this.storage.remove('planetJson');
        }

        this.storage.get('planetJson').then(planetJson => {
            if(planetJson == null)
            {
                this.apiProvider.getFromUrlModel("https://swapi.co/api/planets/").subscribe(planetJson => {  
                    this.storage.set('planetJson', planetJson).then(() => {
                        Helper.LoadPlanetJSon(isReload);
                    });
                });
            }
            else
            {
                Helper.LoadPlanetJSon(isReload);
            }
        });            
    }

    private static LoadPlanetJSon(isReload: boolean) {
        this.storage.get('planetJson').then(planetJson => {
            Variable.planetJson = planetJson;
            
            Variable.planets = [];
            Variable.planetJson.results.forEach(planets => {
                Variable.planets.push(planets);
            });

            let idPage = null;
            if(Variable.planetJson.next != null) {
                idPage = parseInt(Variable.planetJson.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadPlanetsPage(idPage, isReload)
        });
    }

    private static LoadPlanetsPage(index: number, isReload: boolean) {
        if(index != null) {

            let nameStorage = 'planetList' + index.toString();
            console.log(nameStorage);

            if(isReload)
            {
                this.storage.remove(nameStorage);
            }
            
            this.storage.get(nameStorage).then(planetList => {
                if(planetList == null)
                {
                    this.planetProvider.showPlanetList(index).subscribe(planetJson => {
                        
                        this.storage.set(nameStorage, planetJson).then(() => {
                            Helper.LoadPlanets(nameStorage, isReload);
                        });
                    });
                }
                else
                {
                    Helper.LoadPlanets(nameStorage, isReload);
                }
                
            });
        }

       
    }

    public static LoadPlanets(nameStorage: string, isReload: boolean) {
        this.storage.get(nameStorage).then(planet => {
            planet.results.forEach( item => { 
                Variable.planets.push(item);
            });         

            let idPage = null;
            if(planet.next != null) {
                idPage = parseInt(planet.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadPlanetsPage(idPage, isReload)

        });
    }

    public static LoadAllVehicles(isReload: boolean) {
        if(isReload)
        {
            this.storage.remove('vehicleJson');
        }

        
        this.storage.get('vehicleJson').then(vehicleJson => {
            if(vehicleJson == null)
            {
                this.apiProvider.getFromUrlModel("https://swapi.co/api/vehicles/").subscribe(vehicleJson => {  
                    this.storage.set('vehicleJson', vehicleJson).then(() => {
                        Helper.LoadVehicleJSon(isReload);
                    });
                });
            }
            else
            {
                Helper.LoadVehicleJSon(isReload);
            }
        });  
    }

    private static LoadVehicleJSon(isReload: boolean) {

        this.storage.get('vehicleJson').then(vehicleJson => {
            Variable.vehicleJson = vehicleJson;
            
            Variable.vehicles = [];
            Variable.vehicleJson.results.forEach(vehicle => {
                Variable.vehicles.push(vehicle);
            });

            let idPage = null;
            if(Variable.vehicleJson.next != null) {
                idPage = parseInt(Variable.vehicleJson.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadVehiclesPage(idPage, isReload)
        });
    }

    private static LoadVehiclesPage(index: number, isReload: boolean) {
        if(index != null) {
            let nameStorage = 'vehicleList' + index.toString();
            console.log(nameStorage);

            if(isReload)
            {
                this.storage.remove(nameStorage);
            }
            
            this.storage.get(nameStorage).then(vehicleList => {
                if(vehicleList == null)
                {
                    this.vehicleProvider.showVehicleList(index).subscribe(vehicleJson => {
                        
                        this.storage.set(nameStorage, vehicleJson).then(() => {
                            Helper.LoadVehicles(nameStorage, isReload);
                        });
                    });
                }
                else
                {
                    Helper.LoadVehicles(nameStorage, isReload);
                }
                
            });
        }

       
    }

    private static LoadVehicles(nameStorage: string, isReload: boolean) {
        this.storage.get(nameStorage).then(vehicle => {
            vehicle.results.forEach( item => { 
                Variable.vehicles.push(item);
            });         

            let idPage = null;
            if(vehicle.next != null) {
                idPage = parseInt(vehicle.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadVehiclesPage(idPage, isReload)

        });
    }

    public static LoadAllPeople(isReload: boolean) {
        if(isReload)
        {
            this.storage.remove('peopleJson');
        }

        this.storage.get('peopleJson').then(peopleJson => {
            if(peopleJson == null)
            {
                this.apiProvider.getFromUrlModel("https://swapi.co/api/people/").subscribe(peopleJson => {  
                    this.storage.set('peopleJson', peopleJson).then(() => {
                        Helper.LoadPeopleJSon(isReload);
                    });
                });
            }
            else
            {
                Helper.LoadPeopleJSon(isReload);
            }
        });   
    }

    private static LoadPeopleJSon(isReload: boolean) {
        this.storage.get('peopleJson').then(peopleJson => {
            Variable.peopleJson = peopleJson;
            
            Variable.people = [];
            Variable.peopleJson.results.forEach(people => {
                Variable.people.push(people);
            });

            let idPage = null;
            if(Variable.peopleJson.next != null) {
                idPage = parseInt(Variable.peopleJson.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadPeoplePage(idPage, isReload)
        });
    }

    private static LoadPeoplePage(index: number, isReload: boolean) {
        if(index != null) {

            let nameStorage = 'peopleList' + index.toString();
            console.log(nameStorage);

            if(isReload)
            {
                this.storage.remove(nameStorage);
            }
            
            this.storage.get(nameStorage).then(peopleList => {
                if(peopleList == null)
                {
                    this.peopleProvider.showPeopleList(index).subscribe(peopleJson => {
                        
                        this.storage.set(nameStorage, peopleJson).then(() => {
                            Helper.LoadPeople(nameStorage, isReload);
                        });
                    });
                }
                else
                {
                    Helper.LoadPeople(nameStorage, isReload);
                }
                
            });
        }

       
    }

    private static LoadPeople(nameStorage: string, isReload: boolean) {
        this.storage.get(nameStorage).then(people => {
            people.results.forEach( item => { 
                Variable.people.push(item);
            });         

            let idPage = null;
            if(people.next != null) {
                idPage = parseInt(people.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadPeoplePage(idPage, isReload)

        });
    }


    public static LoadAllSpecies(isReload: boolean) {
        if(isReload)
        {
            this.storage.remove('speciesJson');
        }

        this.storage.get('speciesJson').then(speciesJson => {
            if(speciesJson == null)
            {
                this.apiProvider.getFromUrlModel("https://swapi.co/api/species/").subscribe(speciesJson => {  
                    this.storage.set('speciesJson', speciesJson).then(() => {
                        Helper.LoadSpeciesJSon(isReload);
                    });
                });
            }
            else
            {
                Helper.LoadSpeciesJSon(isReload);
            }
        });   
    }

    private static LoadSpeciesJSon(isReload: boolean) {
        this.storage.get('speciesJson').then(speciesJson => {
            Variable.speciesJson = speciesJson;
            
            Variable.species = [];
            Variable.speciesJson.results.forEach(species => {
                Variable.species.push(species);
            });

            let idPage = null;
            if(Variable.speciesJson.next != null) {
                idPage = parseInt(Variable.speciesJson.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadSpeciesPage(idPage, isReload)
        });
    }

    private static LoadSpeciesPage(index: number, isReload: boolean) {
        if(index != null) {

            let nameStorage = 'speciesList' + index.toString();
            console.log(nameStorage);

            if(isReload)
            {
                this.storage.remove(nameStorage);
            }
            
            this.storage.get(nameStorage).then(speciesList => {
                if(speciesList == null)
                {
                    this.speciesProvider.showSpeciesList(index).subscribe(speciesJson => {
                        
                        this.storage.set(nameStorage, speciesJson).then(() => {
                            Helper.LoadSpecies(nameStorage, isReload);
                        });
                    });
                }
                else
                {
                    Helper.LoadSpecies(nameStorage, isReload);
                }
                
            });
        }

       
    }

    private static LoadSpecies(nameStorage: string, isReload: boolean) {
        this.storage.get(nameStorage).then(species => {
            species.results.forEach( item => { 
                Variable.species.push(item);
            });         

            let idPage = null;
            if(species.next != null) {
                idPage = parseInt(species.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadSpeciesPage(idPage, isReload)
        });
    }

    public static LoadAllStarships(isReload: boolean) {
        
        if(isReload)
        {
            this.storage.remove('starshipJson');
        }

        this.storage.get('starshipJson').then(starshipJson => {
            if(starshipJson == null)
            {
                this.apiProvider.getFromUrlModel("https://swapi.co/api/starships/").subscribe(starshipJson => {  
                    this.storage.set('starshipJson', starshipJson).then(() => {
                        Helper.LoadStarshipJSon(isReload);
                    });
                });
            }
            else
            {
                Helper.LoadStarshipJSon(isReload);
            }
        });   
    }
    private static LoadStarshipJSon(isReload: boolean) {
        this.storage.get('starshipJson').then(starshipJson => {
            Variable.starshipJson = starshipJson;
            
            Variable.starships = [];
            Variable.starshipJson.results.forEach(starship => {
                Variable.starships.push(starship);
            });

            let idPage = null;
            if(Variable.starshipJson.next != null) {
                idPage = parseInt(Variable.starshipJson.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadStarshipPage(idPage, isReload)
        });
    }

    private static LoadStarshipPage(index: number, isReload: boolean) {
        if(index != null) {

            let nameStorage = 'starshipList' + index.toString();
            console.log(nameStorage);

            if(isReload)
            {
                this.storage.remove(nameStorage);
            }
            
            this.storage.get(nameStorage).then(starshipList => {
                if(starshipList == null)
                {
                    this.starshipProvider.showStarshipList(index).subscribe(starshipList => {
                        
                        this.storage.set(nameStorage, starshipList).then(() => {
                            Helper.LoadStarship(nameStorage, isReload);
                        });
                    });
                }
                else
                {
                    Helper.LoadStarship(nameStorage, isReload);
                }
                
            });
        }

       
    }

    private static LoadStarship(nameStorage:string, isReload: boolean) {
        this.storage.get(nameStorage).then(starship => {
            starship.results.forEach( item => { 
                Variable.starships.push(item);
            });         

            let idPage = null;
            if(starship.next != null) {
                idPage = parseInt(starship.next.split('/')[5].split('=')[1]);
            }
            
            this.LoadStarshipPage(idPage, isReload)
        });
    }

    public static async LoadAllData() {
        await this.LoadAllFilms(false);
        await this.LoadAllPlanets(false);
        await this.LoadAllVehicles(false);
        await this.LoadAllPeople(false);
        await this.LoadAllSpecies(false);
        await this.LoadAllStarships(false);
        console.log("OK");
    }

    public static GetRandomNumber(min: number, max: number): number {
        return Math.floor(Math.random() * max) + min;
    }
    
    public static RandomizedAnswer(answer1: string, answer2: string, answer3: string, answer4 : string): string[] {
        let paramString : string[] = [answer1, answer2, answer3, answer4];
        let result : string[] = [];

        let i, j, tmp,raw_array = [];
        // create array
        for ( i = 0; i < paramString.length; i++ ) { 
            raw_array.push(i);
        }
        
        // swap elements in array randomly using Fisher-Yates (aka Knuth) Shuffle
        for ( i = paramString.length - 1; i > 0; i-- ) { 
            j = Math.floor( Math.random() * (i + 1) );
            tmp = raw_array[i];
            raw_array[i] = raw_array[j];
            raw_array[j] = tmp;
        }
        raw_array.forEach(element => {
            result.push(paramString[element]);
        });
        return result;
    }
}