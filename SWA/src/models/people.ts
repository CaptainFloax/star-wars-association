import {Being} from './being';

export class People extends Being {
	mass: number;
	height: number;
	birth_year: string;
	gender: string;
	species: string[];
	vehicles: string[];
	starships: string[];

	constructor() {
		super();
		this.mass = null;
		this.height = null;
		this.birth_year = null;
		this.gender = null;
		this.species = null;
		this.vehicles = null;
		this.starships = null;
	}
}