export class Film {
	title: string;
	episode_id: number;
	opening_crawl: string;
	director: string;
	producer: string;
    release_date: string;
    characters: string[];
    planets: string[];
    starships: string[];
    vehicles: string[];
    species: string[];
    url: string;

    constructor() {
        this.title = null;
        this.episode_id = null;
        this.opening_crawl = null;
        this.director = null;
        this.producer = null;
        this.release_date = null;
        this.characters = null;
        this.planets = null;
        this.starships = null;
        this.vehicles = null;
        this.species = null;
        this.url = null;
    }
}